# UK D8 Base Theme

## Configuration

If new config is added, import all from the install defaults:

```
drush cim -y --partial --source=themes/custom/ukd8/config/install/
```